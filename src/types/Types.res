module Country = {
  type t = {
    value: string,
    label: string,
  }
}

module HttpResponse = {
  type t<'data>
  @send external json: t<'data> => Promise.t<'data> = "json"
}