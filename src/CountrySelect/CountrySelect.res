%%raw(`import 'flag-icons/css/flag-icons.min.css'`)

module ReactSelect = ReactSelect.ReactSelect

let makeComponents = () => {
  let components: ReactSelect.components = {
    menuList: props => <MenuList menuProps={props} />,
    option: ({data, innerProps, isFocused, isSelected}) =>
      <Item option={data} innerProps={innerProps} isFocused isSelected />,
    dropdownIndicator: () => React.null,
    indicatorSeparator: () => React.null,
  }
  components
}

@react.component
let make = (~country: option<string>, ~onChange, ~className) => {
  let (dropdownIsOpen, setDropdownIsOpen) = React.useState(_ => false)

  let countries = FetchCountriesHook.fetchCountries()
  let components = makeComponents()

  let onToggleHandler = (_e: ReactEvent.Mouse.t) => setDropdownIsOpen(_ => !dropdownIsOpen)

  let currentCountry = Js.Array2.find(countries, option =>
    switch country {
    | Some(currentCountry) => option.value === currentCountry
    | None => false
    }
  )

  let onChangeHandler = (country: option<Types.Country.t>) => {
    onChange(country)
    setDropdownIsOpen(_ => false)
  }

  let onBlurHandler = (event: ReactEvent.Focus.t) =>
    switch event->ReactEvent.Focus.relatedTarget {
    | Some(_) => ()
    | None => setDropdownIsOpen(_ => false)
    }

  let onKeyDownHandler = (event: ReactEvent.Keyboard.t) => {
    let key = ReactEvent.Keyboard.key(event)
    if key === "Escape" {
      onChange(None)
      setDropdownIsOpen(_ => false)
    }
  }

  let buttonText = switch currentCountry {
  | Some(option) => option.label
  | None => "Select country"
  }

  <div className={className}>
    <Dropdown
      onBlur={onBlurHandler}
      isOpen={dropdownIsOpen}
      target={<Button text={buttonText} onClick={onToggleHandler} />}>
      <ReactSelect
        onKeyDown={onKeyDownHandler}
        value={currentCountry}
        onChange={onChangeHandler}
        options={countries}
        placeholder="Search"
        menuIsOpen={dropdownIsOpen}
        autoFocus={true}
        controlShouldRenderValue={false}
        classNamePrefix="country-select"
        components
        escapeClearsValue={true}
        tabSelectsValue={true}
      />
    </Dropdown>
  </div>
}
