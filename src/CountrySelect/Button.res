@react.component
let make = (~text: string, ~onClick) =>
  <button className="dropbtn" onClick>
    <span className="dropbtn__text"> {text->React.string} </span>
    <Icons.ChevronDown />
  </button>
