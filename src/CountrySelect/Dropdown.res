@react.component
let make = (~children, ~isOpen, ~target, ~onBlur) =>
  <div onBlur={onBlur} className="dropdown">
    target
    {isOpen ? <div className="menu"> {children} </div> : React.null}
  </div>
