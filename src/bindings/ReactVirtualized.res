module List = {
  type rowRendererProps = {index: int, style: ReactDOM.style}
  @module("react-virtualized/dist/commonjs/List") @react.component
  external make: (
    ~width: int,
    ~height: int,
    ~rowCount: int,
    ~rowHeight: rowRendererProps => int,
    ~rowRenderer: rowRendererProps => React.element,
    ~scrollToIndex: int=?,
  ) => React.element = "default"
}

module AutoSizer = {
  type autoSizerProps = {width: int}
  @module("react-virtualized/dist/commonjs/AutoSizer") @react.component
  external make: (
    ~disableHeight: bool,
    ~children: autoSizerProps => React.element,
  ) => React.element = "default"
}
