module ReactSelect = {
  type innerProps<'a> = {data: 'a}
  type innerElement<'a> = {props: innerProps<'a>}
  type selectProps = {value: Js.Nullable.t<Types.Country.t>}
  type menuListProps = {
    maxHeight: int,
    children: React.element,
    selectProps: selectProps,
    options: array<Types.Country.t>,
    innerRef: unit => React.element,
    focusedOption: Js.Nullable.t<Types.Country.t>,
  }
  type optionInnerProps = {
    id: string,
    tabIndex: int,
    onClick: ReactEvent.Mouse.t => unit,
  }
  type optionProps = {
    isFocused: bool,
    isSelected: bool,
    data: Types.Country.t,
    innerProps: optionInnerProps,
  }
  type components = {
    @as("Option")
    option: optionProps => React.element,
    @as("IndicatorSeparator")
    indicatorSeparator: unit => React.element,
    @as("MenuList")
    menuList: menuListProps => React.element,
    @as("DropdownIndicator")
    dropdownIndicator: unit => React.element,
  }

  external castChildtoOption: React.element => innerElement<'a> = "%identity"

  @module("react-select") @react.component
  external make: (
    ~value: option<Types.Country.t>,
    ~options: array<Types.Country.t>,
    ~onChange: option<Types.Country.t> => unit,
    ~onKeyDown: ReactEvent.Keyboard.t => unit,
    ~autoFocus: bool,
    ~controlShouldRenderValue: bool,
    ~menuIsOpen: bool,
    ~placeholder: string,
    ~components: components,
    ~escapeClearsValue: bool,
    ~tabSelectsValue: bool,
    ~classNamePrefix: string=?,
  ) => React.element = "default"
}
