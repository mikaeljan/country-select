module ChevronDown = {
  @module("../assets/chevron_down.svg") @react.component
  external make: unit => React.element = "ReactComponent"
}
