%%raw(`import './App.css';`)
@react.component
let make = () => {
  let (countryCode, setCountryCode) = React.useState(_ => None)

  let handleOnChange = (country: option<Types.Country.t>) =>
    switch country {
    | Some(c) => setCountryCode(_ => Some(c.value))
    | None => setCountryCode(_ => None)
    }

  <div className="app">
    <CountrySelect
      className="enter-your-class"
      country={countryCode}
      onChange={country => handleOnChange(country)}
    />
  </div>
}
