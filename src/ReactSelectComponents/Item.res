module ReactSelect = ReactSelect.ReactSelect

@react.component
let make = (
  ~option: Types.Country.t,
  ~innerProps: ReactSelect.optionInnerProps,
  ~isFocused: bool,
  ~isSelected: bool,
) => {
  <div
    id={innerProps.id}
    onClick={innerProps.onClick}
    tabIndex={innerProps.tabIndex}
    className={`
        dropdown-content__option 
        ${isFocused ? "dropdown-content__option--hovered" : ""} 
        ${isSelected ? "dropdown-content__option--selected" : ""}
      `}>
    <span className={`fi fi-${option.value} dropdown-content__icon`} />
    <span className="dropdown-content__text"> {option.label->React.string} </span>
  </div>
}
