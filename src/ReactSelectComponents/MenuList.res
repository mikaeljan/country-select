module ReactSelect = ReactSelect.ReactSelect
module AutoSizer = ReactVirtualized.AutoSizer
module List = ReactVirtualized.List

@react.component
let make = (~menuProps: ReactSelect.menuListProps) => {
  let childrenArray = React.Children.toArray(menuProps.children)
  let focusedOptionIndex = childrenArray->Js.Array2.findIndex(currChild => {
    let currOption = ReactSelect.castChildtoOption(currChild)
    currOption.props.data === menuProps.focusedOption
  })

  <AutoSizer disableHeight={true}>
    {({width}) =>
      <List
        width
        height={200}
        rowHeight={_ => 32}
        rowCount={childrenArray->Js.Array2.length}
        scrollToIndex={focusedOptionIndex > 0 ? focusedOptionIndex : 0}
        rowRenderer={({index, style}) =>
          <div key={index->Js.Int.toString} style={style}> {childrenArray[index]} </div>}
      />}
  </AutoSizer>
}
