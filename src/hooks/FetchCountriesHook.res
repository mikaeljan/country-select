let countriesUrl = "https://gist.githubusercontent.com/rusty-key/659db3f4566df459bd59c8a53dc9f71f/raw/4127f9550ef063121c564025f6d27dceeb279623/counties.json"

@val external fetch: string =>Promise.t<Types.HttpResponse.t<array<Types.Country.t>>> = "fetch"

let fetchCountries = () => {
  let (countries, setCountries) = React.useState(_ => [])

  React.useEffect0(() => {
    open Promise
    let _ = fetch(countriesUrl)
    ->then(res => {
      res->Types.HttpResponse.json
    })->then(data => {
      setCountries(_ => data)
      data->resolve
    })
    None
  })
  countries
}
