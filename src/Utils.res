module Option = Belt.Option
module Json = Js.Json

let extractProperty = (obj, propertyName) =>
  obj->Js.Dict.get(propertyName)->Option.flatMap(Json.decodeString)->Option.getExn

let decode = json => {
  let obj = json->Json.decodeObject->Belt.Option.getExn
  let value = obj->extractProperty("value")
  let label = obj->extractProperty("label")
  let result: Types.Country.t = {
    value,
    label,
  }
  result
}

let getCountryArray = result => result["data"]->Belt.Array.map(json => json->decode)
