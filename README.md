# Country select

This codebase was created to as a solution for a task, showcasing a skill in FE development using **[ReScript & React](https://rescript-lang.org/docs/react/latest/introduction)**
It features a country-select component using a dataset via REST API request.

Working sample - [Demo](https://mikaeljan.gitlab.io/country-select/)

## What it is?

Basically it is similar to React single-page-application but written in [ReScript](https://rescript-lang.org/) with [React](https://reactjs.org/).

- This project was bootstrapped with Create React App.

## Getting started

To get the app running locally:

```bash
git clone https://github.com/jihchi/rescript-react-realworld-example-app.git
cd country-select
npm install
npm run re:start // <-- keep this process running 
npm start // <-- open separate terminal and run
```

## Possible improvements
- improve App styling approach - currently using one raw .css file for the whole app - for the scale of the project seems a valid choice
- ~~reconsider whether Axios is worth adding~~
- error handling

Then open http://localhost:3000 to see your app.
